// Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause-Clear
#include "qrb_ros_camera/qrb_ros_image_type.hpp"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace qrb_ros_type
{
const void QrbRosImageTypeAdapter::get_ros_data(uint8_t* dst) const
{
  int height = get_ros_message().height;
  int width = get_ros_message().width;
  std::shared_ptr<dmabuf_transport::DmaBuffer> buffer = get_dma_buf();
  buffer->map();
  buffer->sync_start();
  uint8_t* src = (uint8_t*)buffer->addr();

  cv::Mat nv12(height * 1.5, width, CV_8UC1);

  // remove align
  for (uint32_t i = 0; i < height; i++) {
    const uint8_t* tmp_src = src + i * align_width_;
    uint8_t* tmp_dst = nv12.data + i * width;
    memcpy(tmp_dst, tmp_src, width);
  }
  for (uint32_t i = 0; i < height / 2; i++) {
    const uint8_t* tmp_src = src + (i + align_height_) * align_width_;
    uint8_t* tmp_dst = nv12.data + (i + height) * width;
    memcpy(tmp_dst, tmp_src, width);
  }

  // convert nv12 to bgr8
  cv::Mat rgb8(height, width, CV_8UC3, dst);
  cv::cvtColor(nv12, rgb8, cv::COLOR_YUV2BGR_NV12);

  buffer->sync_end();
  buffer->unmap();
}

const void QrbRosImageTypeAdapter::save_ros_data(const uint8_t* src) const
{
  int height = this->get_ros_message().height;
  int width = this->get_ros_message().width;

  // convert bgr8 to yuv420
  uint8_t* tmp_src = const_cast<uint8_t*>(src);
  cv::Mat rgb8(height, width, CV_8UC3, tmp_src);
  cv::Mat yuv420(height * 1.5, width, CV_8UC1);
  cv::cvtColor(rgb8, yuv420, cv::COLOR_BGR2YUV_I420);

  // save data to nv12
  std::shared_ptr<dmabuf_transport::DmaBuffer> buffer = get_dma_buf();
  buffer->map();
  buffer->sync_start();

  uint8_t* nv12_buffer = (uint8_t*)buffer->addr();
  // copy y channel data
  for (uint32_t i = 0; i < height; i++) {
    memcpy(nv12_buffer + i * align_width_, yuv420.data + i * width, width);
  }

  // copy u,v channel data
  uint32_t nLenY = height * width;
  uint32_t nAlignLenY = align_height_ * align_width_;
  uint32_t nLenU = nLenY / 4;
  uint32_t halfWidth = width / 2;
  uint32_t halfHeight = height / 2;

  for (uint32_t j = 0; j < halfHeight; j++) {
    for (uint32_t i = 0; i < halfWidth; i++) {
      uint32_t index = nLenY + j * halfWidth + i;
      nv12_buffer [nAlignLenY + j* align_width_ + 2 * i] = yuv420.data[index];
      nv12_buffer [nAlignLenY + j* align_width_ + 2 * i + 1 ]=
          yuv420.data[index+nLenU];
    }
  }
  buffer->sync_end();
  buffer->unmap();
}

}  // namespace qrb_ros_type
